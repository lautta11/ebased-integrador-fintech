const { updateClientService } = require("../../shared/services/updateClientService");

/** format: YYYYMMDD */
function giftChooser(birth) {
    const birthDate = new Date(birth);
    const month = birthDate.getMonth() + 1;
    const day = birthDate.getDate();
    const season = {
      'summer': { start: 12, end: 3 },
      'autumn': { start: 3, end: 6 },
      'winter': { start: 6, end: 10 },
      'spring': { start: 10, end: 12 },
    };
  
    const gifts = {
      'summer': 'Remera',
      'winter': 'Buzo',
      'autumn': 'Sweater',
      'spring': 'Camisa'
    };
  
    for (const key in season) {
      const { start, end } = season[key];
      if (
        (month === start && day >= 21) ||
        (month > start && month < end) ||
        (month === end && day <= 20)
      ) {
        return gifts[key];
      }
    }
  
    return null;
}

module.exports = async (eventPayload, commandMeta,rawCommand) => {

    console.log("ASSIGN GIFT DOMAIN", eventPayload)
    const payload = JSON.parse(eventPayload.Message);

    const gift = giftChooser(payload.birth)
   
  
    if (gift !== null) {
      payload.gift = gift;
    }
    
    await updateClientService(payload);
  
    console.log("GIFT ASSIGNED")
  
    return {
        body: eventPayload
    };

}


  