const { batchEventMapper } = require("ebased/handler");
const inputMode = require("ebased/handler/input/batchEventQueue");
const outputMode = require('ebased/handler/output/batchEventConfirmation');
const assignGiftDomain = require("../domain/assignGiftDomain");

const retryStrategy = (receiveCount) => 5 * receiveCount;

module.exports.handler = async (events,context) => {
  return batchEventMapper({events,context},inputMode,assignGiftDomain,outputMode,retryStrategy)
};