const { calculateAge } = require("../../shared/utils/calculateAge");
const { ClientCreated } = require("../schema/event/clientEventSchema");
const { ClientSchemaInput } = require("../schema/input/clientSchema");
const { createClientService } = require("../service/createClientService");
const { emitClientCreated } = require("../service/emitClientService");

//const config = require('ebased/util/config');

//const TABLE_NAME = config.get('MENU_TABLE');

/**
 * Logica principal de la lambda
 */
module.exports = async (commandPayload, commandMeta,rawCommand) => {
    // Validation
    console.log("RAWCOMAAND ", rawCommand)
    new ClientSchemaInput(commandPayload,commandMeta)


  if (
      calculateAge(commandPayload.birth) < 18 ||
      calculateAge(commandPayload.birth) > 65
    ) {
      return {
        body: "Client must be between 18 and 65 years old",
      };
    }
  
    await createClientService(commandPayload);
    await emitClientCreated(new ClientCreated(commandPayload, commandMeta))
  
    return {
      body: "Client added succesfully",
    };


}