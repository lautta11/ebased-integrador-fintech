const { updateClientService } = require("../../shared/services/updateClientService");
const { getClienSchema } = require("../schema/input/getClientSchema");

/**
 * Logica principal de la lambda
 */
module.exports = async (commandPayload, commandMeta,rawCommand) => {
    // Validation
    const client = new getClienSchema(commandPayload, commandMeta).get();
    client.status = "INACTIVE"
    await updateClientService(client);  
    return {
      body: "Client deleted successfully",
    };


}