const { getAllClientService } = require("../service/getClientService");

module.exports = async (commandPayload, commandMeta,rawCommand) => {
    const clients = await getAllClientService()

    return {
    statusCode: 200,
    body: clients,
    };


}