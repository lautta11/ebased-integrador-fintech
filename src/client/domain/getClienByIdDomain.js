const { getClienSchema } = require("../schema/input/getClientSchema");
const {  getClientById } = require("../service/getClientService");

module.exports = async (commandPayload, commandMeta,rawCommand) => {
    const client = new getClienSchema(commandPayload, commandMeta).get();
    const clientResponse = await getClientById(client);
    return { status: 200, body: clientResponse };

}