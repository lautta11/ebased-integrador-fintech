const { updateClientService } = require("../../shared/services/updateClientService");
const { calculateAge } = require("../../shared/utils/calculateAge");
const { ClientUpdated } = require("../schema/event/clientEventSchema");
const { ClientSchemaInput } = require("../schema/input/clientSchema");
const { emitClientCreated } = require("../service/emitClientService");

/**
 * Logica principal de la lambda
 */
module.exports = async (commandPayload, commandMeta,rawCommand) => {
    // Validation
    const client = new ClientSchemaInput(commandPayload, commandMeta).get();


  if (
      calculateAge(commandPayload.birth) < 18 ||
      calculateAge(commandPayload.birth) > 65
    ) {
      return {
        body: "Client must be between 18 and 65 years old",
      };
    }
  
    await updateClientService({dni:client.dni,...commandPayload});
    await emitClientCreated(new ClientUpdated(commandPayload, commandMeta))
  
    return {
      body: "Client updated succesfully",
    };


}