const { commandMapper } = require("ebased/handler");
const inputMode = require("ebased/handler/input/commandApi");
const {response,responseError} = require("ebased/handler/output/commandApi");
const deleteClientDomain = require("../domain/deleteClientDomain");
module.exports.handler = async (command,context) => {
    console.log(command)
    console.log(context)
    return commandMapper({command,context},inputMode,deleteClientDomain,{response,responseError})
}