const { commandMapper } = require("ebased/handler");
const inputMode = require("ebased/handler/input/commandApi");
const {response,responseError} = require("ebased/handler/output/commandApi");
const getAllClientDomain = require("../domain/getAllClientDomain");
module.exports.handler = async (command,context) => {
    return commandMapper({command,context},inputMode,getAllClientDomain,{response,responseError})
}