const { commandMapper } = require("ebased/handler");
const inputMode = require("ebased/handler/input/commandApi");
const {response,responseError} = require("ebased/handler/output/commandApi");
const updateClientDomain = require("../domain/updateClientDomain");
module.exports.handler = async (command,context) => {
    console.log(command)
    console.log(context)
    return commandMapper({command,context},inputMode,updateClientDomain,{response,responseError})
}