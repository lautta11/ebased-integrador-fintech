const { InputValidation } = require("ebased/schema/inputValidation");


class ClientSchemaInput extends InputValidation {
    constructor(payload,meta){
        super({
            source: meta.status,
            payload: payload,
            specversion: 'v1.0.0',
            schema:{
                stric: true,
                name: { type: String, required: true },
                lastName: { type: String, required: true },
                dni: { type: String, required: true },
                birth: { type: String, required: true }
            }
        })
    }
}

module.exports = {ClientSchemaInput}