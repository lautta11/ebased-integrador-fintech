const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('tableName');

async function createClientService(data) {
    data.status = 'ACTIVE'
    await dynamo.putItem({
        TableName: TABLE_NAME,
        Item: data,
    })


}

module.exports = { createClientService }