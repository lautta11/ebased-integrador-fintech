const sns = require('ebased/service/downstream/sns');

const emitClientCreated = async (clientCreatedEvent) => {
  const { eventPayload, eventMeta } = clientCreatedEvent.get();
  const snsPublishParams = {
    TopicArn: process.env.createClientTOPIC,
    Message: eventPayload,
  };
  await sns.publish(snsPublishParams, eventMeta);
}

module.exports = { emitClientCreated };