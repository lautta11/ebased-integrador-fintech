const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const { ErrorHandled } = require('ebased/util/error');

const TABLE_NAME = config.get('tableName');

async function getAllClientService() {
    const { Items } = await dynamo.scanTable({
        TableName: TABLE_NAME,
        Limit: 30
      });
    return Items;

}

async function getClientById(client) {
    const { Item } = await dynamo.getItem({
        TableName: TABLE_NAME,
        Key: { dni: client.dni },
      });
      if (!Item) throw new ErrorHandled('Missing Client', { status: 404, code: 'NOT_FOUND' });
      Object.keys(Item).forEach(k => { if (k === 'dni') delete Item[k] });
      return Item;

}

module.exports = { getAllClientService , getClientById}