const { updateClientService } = require("../../shared/services/updateClientService");
const { calculateAge } = require("../../shared/utils/calculateAge");

function randomNumber(minimum, maximum) {
return Math.round(Math.random() * (maximum - minimum) + minimum);
}
  


module.exports = async (eventPayload, commandMeta,rawCommand) => {


    const payload = JSON.parse(eventPayload.Message);

    
    const creditCardNumber = `${randomNumber(0000, 9999)}-${randomNumber(0000, 9999)}-${randomNumber(0000, 9999)}-${randomNumber(0000, 9999)}`
    const expirationDate = `${randomNumber(01, 12)}/${randomNumber(21, 35)}`
    const securityCode = `${randomNumber(000, 999)}`
  
    let type = calculateAge(payload.birth) > 45 ? 'Gold' : 'Classic'

    const card = {...payload, creditCardNumber, expirationDate, securityCode, type}

    console.log("CARD", card)
    
    await updateClientService(card);
  
    console.log("CARD CREATED")
  
    return {
        body: eventPayload
    };

}


  