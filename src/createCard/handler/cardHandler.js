const { batchEventMapper } = require("ebased/handler");
const inputMode = require("ebased/handler/input/batchEventQueue");
const outputMode = require('ebased/handler/output/batchEventConfirmation');
const createCardDomain = require("../domain/createCardDomain");

const retryStrategy = (receiveCount) => 5 * receiveCount;

module.exports.handler = async (events,context) => {
  console.log("EVENTO DE ENTRAADA",events)

  return batchEventMapper({events,context},inputMode,createCardDomain,outputMode,retryStrategy)

};
