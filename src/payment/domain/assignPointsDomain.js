const { getClientById } = require("../../client/service/getClientService");
const { updateClientService } = require("../../shared/services/updateClientService");

function calculatePoints(totalPrice) {
    return Math.floor(totalPrice / 200);
}
  


module.exports = async (eventPayload, commandMeta,rawCommand) => {


    const payload = JSON.parse(eventPayload.Message);

    const client = await getClientById({dni:payload.dni});

    const points = calculatePoints(payload.totalPrice) + (client?.points || 0);
    
    await updateClientService({dni:payload.dni,points});
  
    console.log("Points assigned")
  
    return {
        body: eventPayload
    };

}


  