const { getClientById } = require("../../client/service/getClientService");
const uuid = require('uuid');
const { PaymentSchemaInput } = require("../schema/input/paymentSchema");
const { createPaymentService } = require("../service/paymentService");
const { calculateFinalPrice } = require("../helper/calculateFinalPrice");
const { emitPayment } = require("../service/emitPaymentService");
const { PaymentCreated } = require("../schema/event/paymentEventSchema");


/**
 * Logica principal de la lambda
 */
module.exports = async (commandPayload, commandMeta,rawCommand) => {
    // Validation
    const paymentInput = new PaymentSchemaInput(commandPayload,commandMeta).get()

    const client =  await getClientById({dni:paymentInput.dni})
    if(!client.status || client.status !== "ACTIVE") return { status: 400, body: "Client is inactive" }

    console.log(paymentInput)
    console.log(client)
    let products = []
    let totalPrice = 0
    paymentInput.products.forEach(product => {
        const finalPrice = calculateFinalPrice(product.price, client)
        product.finalPrice = finalPrice
        totalPrice += finalPrice
        products.push(product)
    });

    const payment = {
        id: uuid.v4(),
        dni: paymentInput.dni,
        products,
        totalPrice
    }

    console.log(payment)
    // SK = client#payment  para poder querear todos los payments de un cliente
    await createPaymentService({ pk: payment.id, sk: payment.dni, ...payment });
    await emitPayment(new PaymentCreated(payment, commandMeta))
  
    return {
      body: "Payment created succesfully",
    };


}