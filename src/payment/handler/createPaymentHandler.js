const { commandMapper } = require("ebased/handler");
const inputMode = require("ebased/handler/input/commandApi");
const {response,responseError} = require("ebased/handler/output/commandApi");
const createPaymentDomain = require("../domain/createPaymentDomain");
module.exports.handler = async (command,context) => {
    /**
     * COmmandMApper ya que el evento que invoka la lambda es API Gateway
     * input que recive la lambda
     * modo de input (api)
     * clase que definimos nuestra logica (domain)
     * el output que regresa la lambda JSON
     */
    console.log(command)
    console.log(context)
    return commandMapper({command,context},inputMode,createPaymentDomain,{response,responseError})
}