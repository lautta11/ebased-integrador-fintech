const calculateFinalPrice = (price,client) =>{
    const card = client.type; // classic or gold
    const discount = {
        Classic: 0.08,
        Gold: 0.12
    }; // 0.1 or 0.2
    return price * (1 - discount[card]);
}

module.exports = {calculateFinalPrice}