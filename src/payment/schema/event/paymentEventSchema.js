const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class PaymentCreated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'PAYMENT.CREATE_PAYMENT',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        id: { type: 'uuid/v4', required: true },
        dni: { type: String, required: true },
        products: [{name: { type: String, required: true }, price: { type: Number, required: true }, finalPrice: { type: Number, required: true}}],
        totalPrice: { type: Number, required: true}
      },
    })
  }
}

module.exports = { PaymentCreated  };