const { InputValidation } = require("ebased/schema/inputValidation");
const Schemy = require('schemy');


class PaymentSchemaInput extends InputValidation {
    constructor(payload, meta) {
        super({
            source: meta.status,
            payload: payload,
            specversion: 'v1.0.0',
            schema: {
                stric: true,
                dni: { type: String, required: true },
                products: [{
                    name: { type: String, required: true },
                    price: { type: Number, required: true },
                }],
            }
        })
    }
}


module.exports = { PaymentSchemaInput }