const sns = require('ebased/service/downstream/sns');

const emitPayment = async (paymentCreatedEvent) => {
  const { eventPayload, eventMeta } = paymentCreatedEvent.get();
  const snsPublishParams = {
    TopicArn: process.env.createPaymentTopic,
    Message: eventPayload,
  };
  await sns.publish(snsPublishParams, eventMeta);
}

module.exports = { emitPayment };