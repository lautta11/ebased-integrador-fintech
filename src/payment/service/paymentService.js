const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('paymentTable');

async function createPaymentService(payment) {
    await dynamo.putItem({
        TableName: TABLE_NAME,
        Item: payment,
    })
}

module.exports = { createPaymentService }