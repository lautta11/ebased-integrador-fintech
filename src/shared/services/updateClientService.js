const dynamo = require("ebased/service/storage/dynamo");
const config = require('ebased/util/config');
const { ErrorHandled } = require("ebased/util/error");

const TABLE_NAME = config.get('tableName');

// TODO: hacer getById para chequear que exista cliente por dni ingresado
const updateClientService = async (client) => {
    const params = {
      TableName: TABLE_NAME,
      Key: { dni: client.dni},
      UpdateExpression: 'SET',
      ConditionExpression: 'attribute_exists(dni)',
      ExpressionAttributeNames: {},
      ExpressionAttributeValues: {},
      ReturnValues: 'ALL_NEW',
    }
    // REMOVE PK (DNI)
    const { dni, ...attrToUp} = client
    Object.keys(attrToUp).forEach(key => {
      params.UpdateExpression += ` #${key}=:${key},`;
      params.ExpressionAttributeNames[`#${key}`] = key;
      params.ExpressionAttributeValues[`:${key}`] = client[key];
    });
    params.UpdateExpression = params.UpdateExpression.slice(0, -1);
    console.log("PARAMS BEFORE UPDATE",params);
    
    const { Attributes } = await dynamo.updateItem(params)
    Object.keys(Attributes).forEach(k => { if (k === 'dni') delete Attributes[k] });
    return Attributes;
    

  };

module.exports = { updateClientService }