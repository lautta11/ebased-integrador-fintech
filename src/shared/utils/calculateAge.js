/**
 * 
 * @param {} birthday format YYYYMMDD
 * @returns 
 */
function calculateAge(birthday) {
    const birthDate = new Date(birthday);
    const currentDate = new Date();
    
    // Calculate the difference in years
    const age = currentDate.getFullYear() - birthDate.getFullYear();
    
    // Check if the birthday hasn't occurred yet this year
    const hasBirthdayPassed = (
      currentDate.getMonth() < birthDate.getMonth() ||
      (currentDate.getMonth() === birthDate.getMonth() && currentDate.getDate() < birthDate.getDate())
    );
    
    // Decrease the age by 1 if the birthday hasn't passed yet
    if (!hasBirthdayPassed) {
      return age - 1;
    }
    
    return age;
}

module.exports = {calculateAge}